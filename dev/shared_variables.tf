variable "location" {
  default = "fsn1"
  type    = string
}

variable "datacenter" {
  default = "fsn1-dc14"
  type    = string
}

variable "discourse_volume_id" {
  default = "100641245"
  type    = string
}

variable "loadbalancer_node" {
  type = map(string)
  default = {
    hostname     = "loadbalancer"
    node_name    = "loadbalancer-dev"
    node_type    = "cx22"
    private_ipv4 = "10.0.1.1"
  }
}

variable "server_node" {
  type = map(string)
  default = {
    hostname     = "server"
    node_name    = "server-dev"
    node_type    = "cx22"
    private_ipv4 = "10.0.1.2"
  }
}

variable "client_nodes" {
  type = list(map(string))
  default = [
    {
      hostname     = "client0"
      node_name    = "client0-dev"
      node_type    = "cx22"
      private_ipv4 = "10.0.1.3"
    }
  ]
}

variable "discourse_node" {
  type = map(string)
  default = {
    hostname  = "discourse"
    node_name = "discourse-dev"
    node_type = "cx22"
  }
}
