resource "nomad_csi_volume" "nfs_csi_volumes" {
  for_each = toset(local.nfs_csi_volume_names)

  plugin_id = "nfs"
  volume_id = "${each.key}_vol"
  name      = each.key

  capability {
    access_mode     = "multi-node-multi-writer"
    attachment_mode = "file-system"
  }
  #  capability {
  #    access_mode     = "single-node-writer"
  #    attachment_mode = "file-system"
  #  }
  #  capability {
  #    access_mode     = "multi-node-reader-only"
  #    attachment_mode = "file-system"
  #  }

  parameters = {
    server           = var.nfs_server_ip,
    share            = "${var.nfs_root_share_path}",
    mountPermissions = "0",
    onDelete         = "retain",
  }

  mount_options {
    # mount.nfs: Either use '-o nolock' to keep locks local, or start statd.
    fs_type     = "nfs"
    mount_flags = ["nolock,vers=4.2"]
  }
  depends_on = [time_sleep.wait_for_csi_nfs_jobs]
}
