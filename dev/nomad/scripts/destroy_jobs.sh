#!/usr/bin/env bash

# Destroys all nomad jobs.
# Filtering servers can be done by providing an argument that will be filtered with grep

while read -r job; do
  terraform destroy -auto-approve -target="$job"
done < <(terraform state list | grep nomad_job | grep "$1")
