variable "datacenter" {
  type = string
}
variable "node_hostname" {
  type = string
}
variable "labtest_version" {
  type = string
}

job "labtest" {
  datacenters = ["${var.datacenter}"]
  type        = "service"

  group "labtest" {
    count = 1

    constraint {
      distinct_hosts = true
    }

    network {
      port "http" {}
    }

    vault {}

    restart {
      interval         = "30m"
      attempts         = 3
      delay            = "15s"
      mode             = "fail"
      render_templates = true
    }

    task "labtest" {
      driver = "docker"

      constraint {
        attribute = attr.unique.hostname
        operator  = "=="
        value     = var.node_hostname
      }

      config {
        image = "beehiive/lab-test-server:${var.labtest_version}"
        ports = ["http"]
      }

      env {
        HTTP_PORT = NOMAD_PORT_http
      }

      template {
        data        = <<EOF
{{ with secret "secret/data/default/labtest" }}
LAB_TOKEN="{{ .Data.data.token }}"
{{ end }}
EOF
        destination = "local/file.env"
        env         = true
      }

      service {
        name = "labtest"
        port = "http"

        tags = [
          "traefik.enable=true",

          "traefik.http.routers.labtest-insecure.entrypoints=http",
          "traefik.http.routers.labtest-insecure.rule=Host(`labtest.hiive.buzz`)",
        ]

        check {
          type     = "http"
          path     = "/health_check"
          interval = "30s"
          timeout  = "2s"
        }
      }
      resources {
        cpu    = 100
        memory = 64
      }
    }
  }
}
