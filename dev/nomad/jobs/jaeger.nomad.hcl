variable "datacenter" {
  type = string
}
variable "node_hostname" {
  type = string
}
variable "jaeger_version" {
  type = string
}

job "jaeger" {
  datacenters = ["${var.datacenter}"]
  type        = "service"

  group "jaeger" {
    count = 1

    volume "jaeger" {
      type            = "csi"
      source          = "jaeger_vol"
      read_only       = false
      attachment_mode = "file-system"
      access_mode     = "multi-node-multi-writer"
    }

    network {
      port "query_ui" {
        to = 16686
      }
      port "query_admin" {
        to = 14269
      }
      port "serve_configs" {
        static = 5778
      }
      #port "otlp_grpc" {
      #  static = 4317
      #}
      port "otlp_http" {
        static = 4318
      }
    }

    restart {
      interval         = "30m"
      attempts         = 3
      delay            = "15s"
      mode             = "fail"
      render_templates = true
    }

    task "jaeger" {
      driver = "docker"

      user = "root"

      constraint {
        attribute = attr.unique.hostname
        operator  = "=="
        value     = var.node_hostname
      }

      resources {
        cpu    = 250
        memory = 1024
      }

      volume_mount {
        volume      = "jaeger"
        destination = "/badger"
        read_only   = false
      }

      config {
        image = "jaegertracing/all-in-one:${var.jaeger_version}"

        ports = ["query_ui", "query_admin", "serve_configs", "otlp_http"]

      }

      template {
        data        = <<EOF
SPAN_STORAGE_TYPE=badger
BADGER_EPHEMERAL=false
BADGER_DIRECTORY_VALUE=/badger/data
BADGER_DIRECTORY_KEY=/badger/key
COLLECTOR_OTLP_ENABLED=true
EOF
        destination = "secrets/env"
        env         = true
      }

      service {
        name = "jaeger"
        port = "query_ui"

        tags = [
          "traefik.enable=true",
          "traefik.http.routers.jaeger.entrypoints=https",
          "traefik.http.routers.jaeger.rule=Host(`dev.jaeger.hiive.buzz`)",
          "traefik.http.routers.jaeger.middlewares=traefikauth@file",
          "traefik.http.routers.jaeger.tls=true",
          "traefik.http.routers.jaeger.tls.certresolver=letsEncryptStaging",
          "traefik.http.routers.jaeger.tls.domains[0].main=dev.jaeger.hiive.buzz",

          "traefik.http.routers.jaeger-insecure.entrypoints=http",
          "traefik.http.routers.jaeger-insecure.rule=Host(`dev.jaeger.hiive.buzz`)",
          "traefik.http.routers.jaeger-insecure.middlewares=redirect-https@file",
        ]

        check {
          name     = "jaeger-ui"
          port     = "query_admin"
          type     = "http"
          path     = "/"
          interval = "10s"
          timeout  = "2s"
        }
      }
    }
  }
}
