variable "datacenter" {
  type = string
}
variable "node_hostname" {
  type = string
}

job "echo" {
  datacenters = ["${var.datacenter}"]

  group "echo" {
    count = 1

    network {
      port "http" {}
    }

    restart {
      interval         = "30m"
      attempts         = 3
      delay            = "15s"
      mode             = "fail"
      render_templates = true
    }

    task "echo" {
      constraint {
        attribute = attr.unique.hostname
        operator  = "=="
        value     = var.node_hostname
      }

      env {
        PORT    = NOMAD_PORT_http
        NODE_IP = NOMAD_IP_http
      }

      driver = "docker"

      constraint {
        attribute = attr.unique.hostname
        operator  = "!="
        value     = "server"
      }

      constraint {
        attribute = attr.unique.hostname
        operator  = "!="
        value     = "vault"
      }

      config {
        image = "hashicorp/demo-webapp-lb-guide"
        ports = ["http"]
      }
      service {
        name = "echo"
        port = "http"

        tags = [
          "traefik.enable=true",

          "traefik.http.routers.echo.entrypoints=https",
          "traefik.http.routers.echo.middlewares=traefikauth@file",
          "traefik.http.routers.echo.rule=Host(`dev.echo.hiive.buzz`)",
          "traefik.http.routers.echo.tls=true",
        ]

        check {
          type     = "http"
          path     = "/"
          interval = "2s"
          timeout  = "2s"
        }
      }
      resources {
        cpu    = 300
        memory = 128
      }
    }
  }
}
