variable "datacenter" {
  type = string
}
variable "node_hostname" {
  type = string
}
variable "traefik_version" {
  type = string
}
variable "acme_email" {
  type = string
}

job "traefik" {
  datacenters = ["${var.datacenter}"]
  type        = "service"

  group "traefik" {
    count = 1

    volume "acme_certs" {
      type            = "csi"
      source          = "acme_certs_vol"
      read_only       = false
      attachment_mode = "file-system"
      access_mode     = "multi-node-multi-writer"
    }

    network {
      port "http" {
        static = 80
      }

      port "https" {
        static = 443
      }

      port "postgres" {
        static = 5432
      }

      port "traefik" {
        static = 8081
      }

      port "metrics" {
        static = 8082
      }
    }

    restart {
      interval         = "30m"
      attempts         = 3
      delay            = "15s"
      mode             = "fail"
      render_templates = true
    }

    task "traefik" {
      driver = "docker"

      constraint {
        attribute = attr.unique.hostname
        operator  = "=="
        value     = var.node_hostname
      }

      volume_mount {
        volume      = "acme_certs"
        destination = "/acme_certs"
        read_only   = false
      }

      config {
        image        = "traefik:${var.traefik_version}"
        network_mode = "host"

        volumes = [
          "local/traefik.toml:/etc/traefik/traefik.toml",
          "local/traefik-ui-route.toml:/etc/traefik/dyn/traefik-ui-route.toml",
          "local/nomad-ui-route.toml:/etc/traefik/dyn/nomad-ui-route.toml",
          "local/consul-ui-route.toml:/etc/traefik/dyn/consul-ui-route.toml",
          "local/vault-ui-route.toml:/etc/traefik/dyn/vault-ui-route.toml",
          "local/middlewares.toml:/etc/traefik/dyn/middlewares.toml",
          "local/tls.toml:/etc/traefik/dyn/tls.toml",
          "secrets/hiive_buzz_cert.pem:/private_certs/hiive_buzz_cert.pem",
          "secrets/hiive_buzz_key.pem:/private_certs/hiive_buzz_key.pem",
        ]
      }

      vault {}

      ######################################################################################################################
      #
      # STATIC CONFIGURATION
      #
      ######################################################################################################################
      template {
        data = <<EOF
[api]
  dashboard = true
  debug     = true
  insecure  = true

[log]
  level = "DEBUG"

[accessLog]

[entryPoints]
  [entryPoints.http]
  address = ":80"
  [entryPoints.https]
  address = ":443"
  [entryPoints.postgres]
  address = ":5432"
  [entryPoints.traefik]
  address = ":8081"
  [entryPoints.metrics]
  address = ":8082"

[certificatesResolvers.letsEncrypt.acme]
  email = "${var.acme_email}"
  storage = "/acme_certs/acme.json"
  caServer = "https://acme-v02.api.letsencrypt.org/directory"
  [certificatesResolvers.letsEncrypt.acme.tlsChallenge]

[certificatesResolvers.letsEncryptStaging.acme]
  email = "${var.acme_email}"
  storage = "/acme_certs/acme-staging.json"
  caServer = "https://acme-staging-v02.api.letsencrypt.org/directory"
  [certificatesResolvers.letsEncryptStaging.acme.tlsChallenge]

[providers.file]
  directory = "/etc/traefik/dyn/"

[metrics]
  [metrics.prometheus]
  addEntryPointsLabels = true
  addServicesLabels    = true
  entryPoint           = "metrics"

# Enable Consul Catalog configuration backend.
[providers.consulCatalog]
  prefix           = "traefik"
  exposedByDefault = false

  [providers.consulCatalog.endpoint]
    address = "127.0.0.1:8500"
    scheme  = "http"
EOF

        destination = "local/traefik.toml"
      }

      ######################################################################################################################
      #
      # MIDDLEWARES
      #
      ######################################################################################################################

      template {
        data        = <<EOF
[http.middlewares]
[http.middlewares.redirect-https.redirectScheme]
  scheme = "https"
  permanent = true
        
[http.middlewares.traefikauth.basicAuth]
  users = [
{{ with secret "secret/data/default/traefik" }}
    "{{ .Data.data.traefik_basicauth_user}}:{{ .Data.data.traefik_basicauth_password_hash}}",
{{ end }}
  ]
EOF
        destination = "local/middlewares.toml"
      }

      ######################################################################################################################
      #
      # TRAEFIK ROUTE
      #
      ######################################################################################################################

      template {
        data        = <<EOF
[http]
  [http.routers]
    [http.routers.traefik]
      entryPoints = ["https"]
      rule = "Host(`dev.traefik.hiive.buzz`)"
      middlewares = ["traefikauth"] 
      service = "traefik"
      [http.routers.traefik.tls]
        certResolver = "letsEncryptStaging"
        [[http.routers.nomad.tls.domains]]
          main = "dev.traefik.hiive.buzz"

    [http.routers.traefik-insecure]
      entryPoints = ["http"]
      rule = "Host(`dev.traefik.hiive.buzz`)"
      service = "traefik"
      middlewares = ["redirect-https"] 

  [http.services]
    [http.services.traefik]
      [http.services.traefik.loadBalancer]
        [[http.services.traefik.loadBalancer.servers]]
        url = "http://traefik.service.consul:8081"
EOF
        destination = "local/traefik-ui-route.toml"
      }

      ######################################################################################################################
      #
      # NOMAD ROUTE
      #
      ######################################################################################################################

      template {
        data        = <<EOF
{{ with secret "secret/data/default/traefik" }}
[http]
  [http.routers]
    [http.routers.nomad]
      entryPoints = ["https"]
      rule = "Host(`dev.nomad.hiive.buzz`)"
      middlewares = ["traefikauth"] 
      service = "nomad"
      [http.routers.nomad.tls]
        certResolver = "letsEncryptStaging"
        [[http.routers.nomad.tls.domains]]
          main = "dev.nomad.hiive.buzz"

    [http.routers.nomad-insecure]
      entryPoints = ["http"]
      rule = "Host(`dev.nomad.hiive.buzz`)"
      service = "nomad"
      middlewares = ["redirect-https"] 

  [http.services]
    [http.services.nomad]
      [http.services.nomad.loadBalancer]
        [[http.services.nomad.loadBalancer.servers]]
        url = "http://nomad.service.consul:4646"
{{ end }}
EOF
        destination = "local/nomad-ui-route.toml"
      }

      ######################################################################################################################
      #
      # CONSUL ROUTE
      #
      ######################################################################################################################

      template {
        data        = <<EOF
{{ with secret "secret/data/default/traefik" }}
[http]
  [http.routers]
    [http.routers.consul]
      entryPoints = ["https"]
      rule = "Host(`dev.consul.hiive.buzz`)"
      middlewares = ["traefikauth"] 
      service = "consul"
      [http.routers.consul.tls]
        certResolver = "letsEncryptStaging"
        [[http.routers.consul.tls.domains]]
          main = "dev.consul.hiive.buzz"

    [http.routers.consul-insecure]
      entryPoints = ["http"]
      rule = "Host(`dev.consul.hiive.buzz`)"
      service = "consul"
      middlewares = ["redirect-https"] 

  [http.services]
    [http.services.consul]
      [http.services.consul.loadBalancer]
        [[http.services.consul.loadBalancer.servers]]
        url = "http://consul.service.consul:8500"
{{ end }}
EOF
        destination = "local/consul-ui-route.toml"
      }

      ######################################################################################################################
      #
      # VAULT ROUTE
      #
      ######################################################################################################################

      template {
        data        = <<EOF
{{ with secret "secret/data/default/traefik" }}
[http]
  [http.routers]
    [http.routers.vault]
      entryPoints = ["https"]
      rule = "Host(`dev.vault.hiive.buzz`)"
      middlewares = ["traefikauth"] 
      service = "vault"
      [http.routers.vault.tls]
        certResolver = "letsEncryptStaging"
        [[http.routers.vault.tls.domains]]
          main = "dev.vault.hiive.buzz"

    [http.routers.vault-insecure]
      entryPoints = ["http"]
      rule = "Host(`dev.vault.hiive.buzz`)"
      service = "vault"
      middlewares = ["redirect-https"] 

  [http.services]
    [http.services.vault]
      [http.services.vault.loadBalancer]
        [[http.services.vault.loadBalancer.servers]]
        url = "http://{{ range service "vault" }}{{ .Address }}{{ end }}:8200"
{{ end }}
EOF
        destination = "local/vault-ui-route.toml"
      }

      ######################################################################################################################
      #
      # TLS
      #
      ######################################################################################################################

      template {
        data        = <<EOF
[[tls.certificates]]
  certFile = "/private_certs/hiive_buzz_cert.pem"
  keyFile = "/private_certs/hiive_buzz_key.pem"
  stores = ["default"]
EOF
        destination = "local/tls.toml"
      }

      template {
        data        = <<EOF
{{ with secret "secret/data/default/traefik" }}{{ .Data.data.server_cert }}{{ end }}
EOF
        destination = "secrets/hiive_buzz_cert.pem"
      }

      template {
        data        = <<EOF
{{ with secret "secret/data/default/traefik" }}{{ .Data.data.server_key }}{{ end }}
EOF
        destination = "secrets/hiive_buzz_key.pem"
      }

      service {
        name = "traefik"

        check {
          name     = "alive"
          type     = "tcp"
          port     = "http"
          interval = "10s"
          timeout  = "2s"
        }
      }
    }
  }
}
