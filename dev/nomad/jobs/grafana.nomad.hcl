variable "datacenter" {
  type = string
}
variable "node_hostname" {
  type = string
}
variable "grafana_version" {
  type = string
}
variable "prometheus_version" {
  type = string
}

job "grafana" {
  datacenters = ["${var.datacenter}"]
  type        = "service"

  group "grafana" {
    count = 1

    volume "grafana" {
      type            = "csi"
      source          = "grafana_vol"
      read_only       = false
      attachment_mode = "file-system"
      access_mode     = "multi-node-multi-writer"
    }

    network {
      port "grafana_ui" {
        to = 3000
      }
    }

    restart {
      interval         = "30m"
      attempts         = 3
      delay            = "15s"
      mode             = "fail"
      render_templates = true
    }

    task "grafana" {
      driver = "docker"

      user = "root"

      constraint {
        attribute = attr.unique.hostname
        operator  = "=="
        value     = var.node_hostname
      }

      volume_mount {
        volume      = "grafana"
        destination = "/grafana"
        read_only   = false
      }

      config {
        image = "grafana/grafana:${var.grafana_version}"

        ports = ["grafana_ui"]

        volumes = [
          "local/prometheus_data_source.yaml:/etc/grafana/provisioning/datasources/prometheus_data_source.yaml",
        ]
      }

      vault {}

      template {
        data        = <<EOF
PROMETHEUS_HOST="{{ range service "prometheus"}}{{.Address}}{{ end }}"
PROMETHEUS_PORT="{{ range service "prometheus"}}{{.Port}}{{ end }}"

GF_PATHS_DATA=/grafana/data
GF_AUTH_ANONYMOUS_ORG_ROLE=Admin
GF_AUTH_ANONYMOUS_ENABLED=true
GF_AUTH_BASIC_ENABLED=false
GF_ENABLE_GZIP=true
EOF
        destination = "secrets/env"
        env         = true
      }

      template {
        data        = <<EOF
datasources:
  - name: Prometheus
    type: Prometheus
    url: http://{{ range service "prometheus"}}{{.Address}}{{ end }}:{{ range service "prometheus"}}{{.Port}}{{ end }}
    jsonData:
      prometheusType: Prometheus
      prometheusVersion: ${var.prometheus_version}
      tlsSkipVerify: true
EOF
        destination = "local/prometheus_data_source.yaml"
      }

      service {
        name = "grafana"
        port = "grafana_ui"

        tags = [
          "traefik.enable=true",
          "traefik.http.routers.grafana.entrypoints=https",
          "traefik.http.routers.grafana.rule=Host(`dev.grafana.hiive.buzz`)",
          "traefik.http.routers.grafana.middlewares=traefikauth@file",
          "traefik.http.routers.grafana.tls=true",
          "traefik.http.routers.grafana.tls.certresolver=letsEncryptStaging",
          "traefik.http.routers.grafana.tls.domains[0].main=dev.grafana.hiive.buzz",

          "traefik.http.routers.grafana-insecure.entrypoints=http",
          "traefik.http.routers.grafana-insecure.rule=Host(`dev.grafana.hiive.buzz`)",
          "traefik.http.routers.grafana-insecure.middlewares=redirect-https@file",
        ]

        check {
          name     = "grafana_ui"
          type     = "http"
          path     = "/api/health"
          interval = "10s"
          timeout  = "2s"
        }
      }
    }
  }
}
