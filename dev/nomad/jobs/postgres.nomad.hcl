variable "datacenter" {
  type = string
}
variable "node_hostname" {
  type = string
}
variable "postgres_version" {
  type = string
}

job "postgres" {

  datacenters = ["${var.datacenter}"]

  group "postgres" {
    count = 1

    volume "postgres" {
      type            = "csi"
      source          = "postgres_vol"
      read_only       = false
      attachment_mode = "file-system"
      access_mode     = "multi-node-multi-writer"
    }

    network {
      port "pg" { to = 5432 }
    }

    vault {}

    restart {
      interval         = "30m"
      attempts         = 3
      delay            = "15s"
      mode             = "fail"
      render_templates = true
    }

    task "postgres" {
      driver = "docker"

      constraint {
        attribute = attr.unique.hostname
        operator  = "=="
        value     = var.node_hostname
      }

      config {
        image = "postgres:${var.postgres_version}"
        ports = ["pg"]

        volumes = [
          "local/create_users.sh:/docker-entrypoint-initdb.d/10-create_users.sh",
          "local/create_dbs.sh:/docker-entrypoint-initdb.d/20-create_dbs.sh",
        ]
      }
      template {
        data        = <<EOF
{{ with secret "secret/data/default/postgres" }}
#!/bin/sh
echo 1 > /tmp/createusers
psql -c "CREATE USER {{ .Data.data.hiive_username }} WITH PASSWORD '{{ .Data.data.hiive_password }}';"
{{ end }}
EOF
        destination = "local/create_users.sh"
      }

      template {
        data        = <<EOF
{{ with secret "secret/data/default/postgres" }}
#!/bin/sh
echo 1 > /tmp/createdbs
createdb -U postgres -O {{ .Data.data.hiive_username }} hiive
{{ end }}
EOF
        destination = "local/create_dbs.sh"
      }
      template {
        data        = <<EOF
{{ with secret "secret/data/default/postgres" }}
POSTGRES_USER="{{ .Data.data.pg_username }}"
POSTGRES_PASSWORD="{{ .Data.data.pg_password }}"
{{ end }}
EOF
        destination = "secrets/file.env"
        env         = true
      }

      volume_mount {
        volume      = "postgres"
        destination = "/var/lib/postgresql/data"
        read_only   = false
      }

      service {
        name = "postgres"
        port = "pg"

        tags = [
          "traefik.enable=true",
          "traefik.tcp.routers.postgres.entrypoints=postgres",
          "traefik.tcp.routers.postgres.rule=HostSNI(`*`)",
        ]

        check {
          task     = "postgres"
          type     = "script"
          command  = "pg_isready"
          args     = ["-U", "postgres"]
          interval = "30s"
          timeout  = "5s"
        }
      }
    }
  }
}
