variable "datacenter" {
  type = string
}
variable "node_hostname" {
  type = string
}
variable "prometheus_version" {
  type = string
}

job "prometheus" {
  datacenters = ["${var.datacenter}"]
  type        = "service"

  group "prometheus" {
    count = 1

    volume "prometheus" {
      type            = "csi"
      source          = "prometheus_vol"
      read_only       = false
      attachment_mode = "file-system"
      access_mode     = "multi-node-multi-writer"
    }

    network {
      port "prometheus_ui" {
        to = 9090
      }
    }

    restart {
      interval         = "30m"
      attempts         = 3
      delay            = "15s"
      mode             = "fail"
      render_templates = true
    }

    task "prometheus" {
      driver = "docker"

      user = "root"

      constraint {
        attribute = attr.unique.hostname
        operator  = "=="
        value     = var.node_hostname
      }

      volume_mount {
        volume      = "prometheus"
        destination = "/prometheus"
        read_only   = false
      }

      constraint {
        attribute = attr.unique.hostname
        operator  = "!="
        value     = "vault"
      }

      config {
        image = "prom/prometheus:${var.prometheus_version}"

        volumes = [
          "local/prometheus.yml:/etc/prometheus/prometheus.yml",
        ]

        ports = ["prometheus_ui"]
      }

      template {
        data = <<EOF
global:
  scrape_interval: 10s

scrape_configs:
  - job_name: 'prometheus_ui'
    scrape_interval: 5s
    static_configs:
      - targets: ['localhost:9090']

  - job_name: 'traefik'
    static_configs:
      - targets: ['traefik.service.consul:8082']

  - job_name: "node_exporter"
    metrics_path: "/metrics"
    consul_sd_configs:
      - server: "10.0.1.2:8500"
        services:
          - "prometheus-node-exporter"
EOF

        destination = "local/prometheus.yml"
      }

      service {
        name = "prometheus"
        port = "prometheus_ui"

        tags = [
          "traefik.enable=true",
          "traefik.http.routers.prometheus.entrypoints=https",
          "traefik.http.routers.prometheus.rule=Host(`dev.prometheus.hiive.buzz`)",
          "traefik.http.routers.prometheus.middlewares=traefikauth@file",
          "traefik.http.routers.prometheus.tls=true",
          "traefik.http.routers.prometheus.tls.certresolver=letsEncryptStaging",
          "traefik.http.routers.prometheus.tls.domains[0].main=dev.prometheus.hiive.buzz",

          "traefik.http.routers.prometheus-insecure.entrypoints=http",
          "traefik.http.routers.prometheus-insecure.rule=Host(`dev.prometheus.hiive.buzz`)",
          "traefik.http.routers.prometheus-insecure.middlewares=redirect-https@file",
        ]

        check {
          name     = "prometheus_ui"
          type     = "http"
          path     = "/-/healthy"
          interval = "10s"
          timeout  = "2s"
        }
      }
    }
  }
}

