variable "datacenter" {
  type = string
}
variable "node_exporter_version" {
  type = string
}

job "prometheus_node_exporters" {
  datacenters = ["${var.datacenter}"]
  type        = "system"

  group "prometheus_node_exporter" {

    network {
      mode = "bridge"
      port "http" {
        to = 9100
      }
    }

    restart {
      interval         = "30m"
      attempts         = 3
      delay            = "15s"
      mode             = "fail"
      render_templates = true
    }

    task "prometheus_node_exporter" {
      driver = "docker"

      config {
        image    = "prom/node-exporter:${var.node_exporter_version}"
        args     = ["--path.rootfs=/host"]
        pid_mode = "host"

        volumes = [
          "/:/host:ro,rslave",
        ]
      }

      service {
        name = "prometheus-node-exporter"
        port = "http"

        check {
          type     = "tcp"
          interval = "3s"
          timeout  = "1s"
        }
      }
    }
  }
}

