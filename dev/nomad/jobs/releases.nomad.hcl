variable "datacenter" {
  type = string
}
variable "node_hostname" {
  type = string
}
variable "releases_version" {
  type = string
}

job "releases" {
  datacenters = ["${var.datacenter}"]
  type        = "service"

  group "releases" {
    count = 1

    constraint {
      distinct_hosts = true
    }

    volume "releases" {
      type            = "csi"
      source          = "releases_vol"
      read_only       = true
      attachment_mode = "file-system"
      access_mode     = "multi-node-multi-writer"
    }

    network {
      port "http" {}
    }

    restart {
      interval         = "30m"
      attempts         = 3
      delay            = "15s"
      mode             = "fail"
      render_templates = true
    }

    task "releases" {
      driver = "docker"

      constraint {
        attribute = attr.unique.hostname
        operator  = "=="
        value     = var.node_hostname
      }

      volume_mount {
        volume      = "releases"
        destination = "/http_root"
        read_only   = true
      }

      config {
        image = "beehiive/dev-hiive-releases:${var.releases_version}"
        ports = ["http"]
      }

      env {
        APP_ENVIRONMENT          = "development"
        ROOT_DIR                 = "/http_root"
        APP_APPLICATION__PORT    = NOMAD_PORT_http
        APP_OTEL_COLLECTOR__PORT = "4318"
      }

      template {
        data        = <<EOF
APP_OTEL_COLLECTOR__HOST="{{ range service "jaeger"}}{{.Address}}{{ end }}"
EOF
        destination = "local/file.env"
        env         = true
      }

      service {
        name = "releases"
        port = "http"

        tags = [
          "traefik.enable=true",

          "traefik.http.routers.releases-insecure.entrypoints=http",
          "traefik.http.routers.releases-insecure.middlewares=traefikauth@file",
          "traefik.http.routers.releases-insecure.rule=Host(`dev.releases.hiive.buzz`)",

          "traefik.http.routers.releases.entrypoints=https",
          "traefik.http.routers.releases.middlewares=traefikauth@file",
          "traefik.http.routers.releases.rule=Host(`dev.releases.hiive.buzz`)",
          "traefik.http.routers.releases.tls=true",

          "traefik.http.routers.deeplink-hijack.entrypoints=https",
          "traefik.http.routers.deeplink-hijack.rule=Host(`dev.api.hiive.buzz`) && (Path(`/.well-known/assetlinks.json`) || Path(`/.well-known/apple-app-site-association`))",
          "traefik.http.routers.deeplink-hijack.tls=true",
          "traefik.http.routers.deeplink-hijack.tls.certresolver=letsEncrypt",
          "traefik.http.routers.deeplink-hijack.tls.domains[0].main=dev.api.hiive.buzz",
        ]

        check {
          type     = "http"
          path     = "/health_check"
          interval = "30s"
          timeout  = "2s"
        }
      }
      resources {
        cpu    = 300
        memory = 128
      }
    }
  }
}
