variable "datacenter" {
  type = string
}
variable "node_hostname" {
  type = string
}
variable "zipkin_version" {
  type = string
}


job "zipkin" {
  datacenters = ["${var.datacenter}"]

  group "zipkin" {
    count = 1

    network {
      port "http" {
        to = 9411
      }
    }

    restart {
      interval         = "30m"
      attempts         = 3
      delay            = "15s"
      mode             = "fail"
      render_templates = true
    }

    task "zipkin" {
      driver = "docker"

      constraint {
        attribute = attr.unique.hostname
        operator  = "=="
        value     = var.node_hostname
      }

      config {
        image = "openzipkin/zipkin:${var.zipkin_version}"
        ports = ["http"]
      }

      vault {}

      service {
        name = "zipkin"
        port = "http"

        tags = [
          "traefik.enable=true",
          "traefik.http.routers.zipkin.entrypoints=https",
          "traefik.http.routers.zipkin.rule=Host(`dev.zipkin.hiive.buzz`)",
          "traefik.http.routers.zipkin.middlewares=traefikauth@file",
          "traefik.http.routers.zipkin.tls=true",
          "traefik.http.routers.zipkin.tls.certresolver=letsEncryptStaging",
          "traefik.http.routers.zipkin.tls.domains[0].main=dev.zipkin.hiive.buzz",

          "traefik.http.routers.zipkin-insecure.entrypoints=http",
          "traefik.http.routers.zipkin-insecure.rule=Host(`dev.zipkin.hiive.buzz`)",
          "traefik.http.routers.zipkin-insecure.middlewares=redirect-https@file",
        ]

        check {
          type     = "http"
          path     = "/health"
          interval = "30s"
          timeout  = "2s"
        }
      }
    }
  }
}
