variable "datacenter" {
  type = string
}
variable "node_hostname" {
  type = string
}
variable "apidoc_version" {
  type = string
}

job "apidoc" {
  datacenters = ["${var.datacenter}"]
  type        = "service"

  group "apidoc" {
    count = 1

    constraint {
      distinct_hosts = true
    }

    network {
      port "http" {
        to = 80
      }
    }

    restart {
      interval         = "30m"
      attempts         = 3
      delay            = "15s"
      mode             = "fail"
      render_templates = true
    }

    task "apidoc" {
      driver = "docker"

      constraint {
        attribute = attr.unique.hostname
        operator  = "=="
        value     = var.node_hostname
      }

      config {
        image = "beehiive/dev-hiive-apidoc:${var.apidoc_version}"

        volumes = [
          "local/swagger-initializer.js:/public/swagger-initializer.js"
        ]
        ports = ["http"]
      }

      template {
        data = <<EOF
window.onload = function() {
  window.ui = SwaggerUIBundle({
    url: "https://dev.apidoc.hiive.buzz/openapi.yaml",
    dom_id: '#swagger-ui',
    supportedSubmitMethods: []
  });
};
EOF

        destination = "local/swagger-initializer.js"
      }

      service {
        name = "apidoc"
        port = "http"

        tags = [
          "traefik.enable=true",
          "traefik.http.routers.apidoc.entrypoints=https",
          "traefik.http.routers.apidoc.rule=Host(`dev.apidoc.hiive.buzz`)",
          "traefik.http.routers.apidoc.middlewares=traefikauth@file",
          "traefik.http.routers.apidoc.tls=true",
          "traefik.http.routers.apidoc.tls.certresolver=letsEncrypt",
          "traefik.http.routers.apidoc.tls.domains[0].main=dev.apidoc.hiive.buzz",

          "traefik.http.routers.apidoc-insecure.entrypoints=http",
          "traefik.http.routers.apidoc-insecure.rule=Host(`dev.apidoc.hiive.buzz`)",
          "traefik.http.routers.apidoc-insecure.middlewares=redirect-https@file",
        ]

        check {
          type     = "http"
          path     = "/"
          interval = "30s"
          timeout  = "2s"
        }
      }
      resources {
        cpu    = 300
        memory = 128
      }
    }
  }
}
