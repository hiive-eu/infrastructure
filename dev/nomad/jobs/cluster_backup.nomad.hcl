variable "datacenter" {
  type = string
}
variable "node_hostname" {
  type = string
}
variable "cluster_backup_version" {
  type = string
}
variable "backup_volume_name" {
  type = string
}
variable "root_dir_volume_mounts" {
  default = "/backup_dirs/"
  type    = string
}
variable "num_local_backup_archives" {
  type = string
}
variable "num_remote_backup_archives" {
  type = string
}
variable "s3_config_path" {
  type = string
}
variable "bucket_name" {
  type = string
}
variable "volumes_to_backup" {
  type = list(string)
}
variable "bucket_location" {
  type = string
}
variable "host_base" {
  type = string
}
variable "host_bucket" {
  type = string
}
variable "website_endpoint" {
  type = string
}

job "cluster_backup" {
  datacenters = ["${var.datacenter}"]

  type = "batch"

  periodic {
    crons            = ["0 2 * * SUN"]
    prohibit_overlap = true
  }

  group "cluster_backup" {
    count = 1

    vault {}

    # FOR LOOP
    dynamic "volume" {
      for_each = var.volumes_to_backup
      labels   = [volume.value]

      content {
        type            = "csi"
        source          = "${volume.value}_vol"
        attachment_mode = "file-system"
        access_mode     = "multi-node-multi-writer"
      }
    }

    volume "backups" {
      type            = "csi"
      source          = "${var.backup_volume_name}_vol"
      attachment_mode = "file-system"
      access_mode     = "multi-node-multi-writer"
    }


    task "cluster_backup" {
      driver = "docker"

      constraint {
        attribute = attr.unique.hostname
        operator  = "=="
        value     = var.node_hostname
      }

      # FOR LOOP
      dynamic "volume_mount" {
        for_each = var.volumes_to_backup

        content {
          volume      = volume_mount.value
          destination = "${var.root_dir_volume_mounts}${volume_mount.value}"
        }
      }
      volume_mount {
        volume      = var.backup_volume_name
        destination = "${var.root_dir_volume_mounts}${var.backup_volume_name}"
      }
      #

      template {
        data        = <<EOF
{{ with secret "secret/data/default/cluster_backup" }}
[default]
access_key = {{ .Data.data.wasabi_access_key }}
bucket_location = ${var.bucket_location}
host_base = ${var.host_base}
host_bucket = ${var.host_bucket}
secret_key = {{ .Data.data.wasabi_secret_key }}
website_endpoint = ${var.website_endpoint}
{{ end }}
EOF
        destination = "local/.s3cfg"
      }

      config {
        image = "beehiive/dev-cluster-backup:${var.cluster_backup_version}"
        volumes = [
          "local/.s3cfg:/.s3cfg",
        ]
      }

      env {
        ROOT_DIR_VOLUME_MOUNTS = var.root_dir_volume_mounts
        BACKUP_VOLUME_NAME     = var.backup_volume_name
        BACKUP_DIRS            = join(" ", var.volumes_to_backup)
        S3_CONFIG_PATH         = var.s3_config_path
        BUCKET_NAME            = var.bucket_name
        NUM_LOCAL_BACKUPS      = var.num_local_backup_archives
        NUM_REMOTE_BACKUPS     = var.num_remote_backup_archives
      }
    }
  }
}
