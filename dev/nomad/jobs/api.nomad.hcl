variable "datacenter" {
  type = string
}
variable "api_version" {
  type = string
}
variable "node_hostname" {
  type = string
}

job "api" {
  datacenters = ["${var.datacenter}"]

  group "api" {
    count = 1

    network {
      port "http" {}
    }

    restart {
      interval         = "30m"
      attempts         = 3
      delay            = "15s"
      mode             = "fail"
      render_templates = true
    }

    task "api" {
      driver = "docker"

      constraint {
        attribute = attr.unique.hostname
        operator  = "=="
        value     = var.node_hostname
      }

      config {
        image = "beehiive/dev-hiive-api:${var.api_version}"
        ports = ["http"]
      }

      vault {}

      env {
        APP_ENVIRONMENT                         = "development"
        APP_APPLICATION__PORT                   = NOMAD_PORT_http
        APP_DATABASE__DATABASE_NAME             = "hiive"
        APP_DATABASE__REQUIRE_SSL               = false
        APP_APPLICATION__ACCESS_TOKEN_DURATION  = 5
        APP_APPLICATION__REFRESH_TOKEN_DURATION = 10
        APP_APPLICATION__BASE_URL               = "https://dev.api.hiive.buzz"
        APP_EMAIL_CLIENT__SENDER_EMAIL          = "api@hiive.buzz"
        APP_OTEL_COLLECTOR__PORT                = "4318"
      }

      template {
        data        = <<EOF
{{ with secret "secret/data/default/api" }}
APP_APPLICATION__SECRET_KEY="{{ .Data.data.secretkey }}"
APP_DATABASE__USERNAME="{{ .Data.data.pg_username }}"
APP_DATABASE__PASSWORD="{{ .Data.data.pg_password }}"
APP_DATABASE__HOST="{{ range service "postgres"}}{{.Address}}{{ end }}"
APP_DATABASE__PORT="{{ range service "postgres"}}{{.Port}}{{ end }}"
APP_EMAIL_CLIENT__AUTHORIZATION_TOKEN="{{ .Data.data.brevo_token }}"
APP_OTEL_COLLECTOR__HOST="{{ range service "jaeger"}}{{.Address}}{{ end }}"
{{ end }}
EOF
        destination = "secrets/file.env"
        env         = true
      }

      service {
        name = "api"
        port = "http"

        tags = [
          "traefik.enable=true",
          "traefik.http.routers.api.entrypoints=https",
          "traefik.http.routers.api.rule=Host(`dev.api.hiive.buzz`)",
          "traefik.http.routers.api.middlewares=traefikauth@file",
          "traefik.http.routers.api.tls=true",
          "traefik.http.routers.api.tls.certresolver=letsEncrypt",
          "traefik.http.routers.api.tls.domains[0].main=dev.api.hiive.buzz",

          "traefik.http.routers.api-insecure.entrypoints=http",
          "traefik.http.routers.api-insecure.rule=Host(`dev.api.hiive.buzz`)",
          "traefik.http.routers.api-insecure.middlewares=redirect-https@file",

          "traefik.http.routers.api-readings.entrypoints=https",
          "traefik.http.routers.api-readings.middlewares=traefikauth@file",
          "traefik.http.routers.api-readings.rule=Host(`dev.readings.hiive.buzz`) && (Path(`/reading`) || Path(`/gwtest`))",
          "traefik.http.routers.api-readings.tls=true",
          "traefik.http.routers.api-readings.rule=Host(`dev.readings.hiive.buzz`) && (Path(`/reading`) || Path(`/gwtest`))",
        ]

        check {
          type     = "http"
          path     = "/health_check"
          interval = "30s"
          timeout  = "2s"
        }
      }
    }
  }
}


