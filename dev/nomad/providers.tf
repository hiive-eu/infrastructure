terraform {
  required_providers {
    null = {
      source  = "hashicorp/null"
      version = "3.2.2"
    }
    nomad = {
      source  = "hashicorp/nomad"
      version = "2.2.0"
    }
  }
}
