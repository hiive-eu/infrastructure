# Set up ssh port forwarding:
# ssh -L 127.0.0.1:4646:127.0.0.1:4646 -N root@hiive.buzz
provider "nomad" {
  address = "http://127.0.0.1:4646"
}

resource "nomad_job" "csi-nfs-controller" {
  jobspec = file("jobs/csi_nfs_controller.nomad.hcl")

  hcl2 {
    vars = {
      datacenter    = var.datacenter
      node_hostname = var.server_node["hostname"]
    }
  }
}

resource "nomad_job" "csi_nfs_node" {
  jobspec = file("jobs/csi_nfs_node.nomad.hcl")

  hcl2 {
    vars = {
      datacenter = var.datacenter
    }
  }
  depends_on = [nomad_job.csi-nfs-controller]
}

resource "nomad_job" "prometheus_node_exporters" {
  jobspec = file("jobs/prometheus_node_exporters.nomad.hcl")

  hcl2 {
    vars = {
      datacenter            = var.datacenter
      node_exporter_version = var.node_exporter_version
    }
  }
}

resource "nomad_job" "jaeger_job" {
  jobspec = file("jobs/jaeger.nomad.hcl")

  hcl2 {
    vars = {
      datacenter     = var.datacenter
      node_hostname  = var.loadbalancer_node["hostname"]
      jaeger_version = var.jaeger_version
    }
  }

  depends_on = [time_sleep.wait_for_volumes]
}

resource "nomad_job" "traefik_job" {
  jobspec = file("jobs/traefik.nomad.hcl")

  hcl2 {
    vars = {
      datacenter      = var.datacenter
      node_hostname   = var.loadbalancer_node["hostname"]
      traefik_version = var.traefik_version,
      acme_email      = var.acme_email,
    }
  }

  depends_on = [time_sleep.wait_for_volumes, time_sleep.wait_for_jaeger]
}

resource "nomad_job" "prometheus_job" {
  jobspec = file("jobs/prometheus.nomad.hcl")

  hcl2 {
    vars = {
      datacenter         = var.datacenter
      node_hostname      = var.client_nodes[0]["hostname"]
      prometheus_version = var.prometheus_version
    }
  }

  depends_on = [time_sleep.wait_for_traefik, nomad_job.prometheus_node_exporters]
}

resource "nomad_job" "grafana_job" {
  jobspec = file("jobs/grafana.nomad.hcl")

  hcl2 {
    vars = {
      datacenter         = var.datacenter
      node_hostname      = var.client_nodes[0]["hostname"]
      grafana_version    = var.grafana_version
      prometheus_version = var.prometheus_version
    }
  }

  depends_on = [time_sleep.wait_for_traefik, time_sleep.wait_for_prometheus]
}

resource "nomad_job" "postgres_job" {
  jobspec = file("jobs/postgres.nomad.hcl")

  hcl2 {
    vars = {
      datacenter       = var.datacenter
      node_hostname    = var.server_node["hostname"]
      postgres_version = var.postgres_version
    }
  }

  depends_on = [time_sleep.wait_for_traefik]
}

resource "nomad_job" "api_job" {
  jobspec = file("jobs/api.nomad.hcl")

  hcl2 {
    vars = {
      datacenter    = var.datacenter
      node_hostname = var.client_nodes[0]["hostname"]
      api_version   = var.api_version
    }
  }

  depends_on = [time_sleep.wait_for_traefik, time_sleep.wait_for_jaeger, time_sleep.wait_for_postgres]
}

resource "nomad_job" "apidoc_job" {
  jobspec = file("jobs/apidoc.nomad.hcl")

  hcl2 {
    vars = {
      datacenter     = var.datacenter
      node_hostname  = var.client_nodes[0]["hostname"]
      apidoc_version = var.apidoc_version
    }
  }

  depends_on = [time_sleep.wait_for_traefik]
}

resource "nomad_job" "releases_job" {
  jobspec = file("jobs/releases.nomad.hcl")

  hcl2 {
    vars = {
      datacenter       = var.datacenter
      node_hostname    = var.client_nodes[0]["hostname"]
      releases_version = var.releases_version
    }
  }

  depends_on = [time_sleep.wait_for_traefik]
}

resource "nomad_job" "cluster_backup_job" {
  jobspec = file("jobs/cluster_backup.nomad.hcl")

  hcl2 {
    vars = {
      datacenter                 = var.datacenter
      node_hostname              = var.server_node["hostname"]
      cluster_backup_version     = var.cluster_backup_version
      backup_volume_name         = var.backup_volume_name
      num_local_backup_archives  = var.num_local_backup_archives
      num_remote_backup_archives = var.num_remote_backup_archives
      volumes_to_backup          = "[ ${join(", ", [for v in var.volumes_to_backup : format("%q", v)])} ]"
      s3_config_path             = var.s3_config_path
      bucket_name                = var.wasabi_backup_bucket_name
      bucket_location            = var.wasabi_bucket_location
      host_base                  = var.wasabi_host_base
      host_bucket                = var.wasabi_host_bucket
      website_endpoint           = var.wasabi_website_endpoint
    }
  }

  depends_on = [time_sleep.wait_for_traefik]
}

resource "nomad_job" "labtest_job" {
  jobspec = file("jobs/labtest.nomad.hcl")

  hcl2 {
    vars = {
      datacenter      = var.datacenter
      node_hostname   = var.client_nodes[0]["hostname"]
      labtest_version = var.labtest_version
    }
  }

  depends_on = [time_sleep.wait_for_traefik]
}

resource "time_sleep" "wait_for_csi_nfs_jobs" {
  depends_on = [nomad_job.csi-nfs-controller, nomad_job.csi_nfs_node]

  create_duration  = "10s"
  destroy_duration = "10s"
}

resource "time_sleep" "wait_for_volumes" {
  depends_on = [nomad_csi_volume.nfs_csi_volumes]

  create_duration  = "10s"
  destroy_duration = "10s"
}

resource "time_sleep" "wait_for_jaeger" {
  depends_on = [nomad_job.jaeger_job]

  create_duration  = "30s"
  destroy_duration = "10s"
}

resource "time_sleep" "wait_for_traefik" {
  depends_on = [nomad_job.traefik_job]

  create_duration  = "30s"
  destroy_duration = "10s"
}

resource "time_sleep" "wait_for_postgres" {
  depends_on = [nomad_job.postgres_job]

  create_duration  = "30s"
  destroy_duration = "10s"
}

resource "time_sleep" "wait_for_prometheus" {
  depends_on = [nomad_job.prometheus_job]

  create_duration  = "30s"
  destroy_duration = "10s"
}
