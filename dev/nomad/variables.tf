locals {
  nfs_csi_volume_names = ["acme_certs", "prometheus", "grafana", "postgres", "releases", "vault", "jaeger", "${var.backup_volume_name}"]
}

variable "volumes_to_backup" {
  default = ["acme_certs", "prometheus", "grafana", "releases", "vault", "jaeger"]
  type    = list(string)
}

variable "backup_volume_name" {
  default = "backups"
}

# NFS
variable "nfs_server_ip" {
  default = "10.0.1.2"
  type    = string
}
variable "nfs_root_share_path" {
  default = "/nfs"
  type    = string
}

# Traefik
variable "traefik_version" {
  default = "v3.1.2"
  type    = string
}
variable "acme_email" {
  default = "anders@hiive.eu"
  type    = string
}

# Prometheus
variable "node_exporter_version" {
  default = "v1.8.2"
  type    = string
}
variable "prometheus_version" {
  default = "v2.54.1"
  type    = string
}

# Grafana
variable "grafana_version" {
  default = "11.2.0"
  type    = string
}

# Zipkin
#variable "zipkin_version" {
#  default = "2.24.3"
#  type    = string
#}

# Jaeger
variable "jaeger_version" {
  default = "1.60"
  type    = string
}

# Postgres
variable "postgres_version" {
  default = "16.4"
  type    = string
}

# HIIVE
variable "api_version" {
  default = "latest"
  type    = string
}

variable "apidoc_version" {
  default = "latest"
  type    = string
}

variable "releases_version" {
  default = "latest"
  type    = string
}

variable "labtest_version" {
  default = "latest"
  type    = string
}

variable "cluster_backup_version" {
  default = "latest"
  type    = string
}

# Backup
variable "wasabi_bucket_location" {
  default = "eu-central-2"
  type    = string
}
variable "wasabi_host_base" {
  default = "s3.eu-central-2.wasabisys.com"
  type    = string
}
variable "wasabi_host_bucket" {
  default = "%(bucket)s.s3.eu-central-2.wasabisys.com"
  type    = string
}
variable "wasabi_website_endpoint" {
  default = "http://%(bucket)s.s3-website-%(location)s.amazonaws.com/"
  type    = string
}
variable "s3_config_path" {
  default = "/.s3cfg"
  type    = string
}
variable "wasabi_backup_bucket_name" {
  default = "dev-backup"
  type    = string
}
variable "num_local_backup_archives" {
  default = 2
  type    = number
}
variable "num_remote_backup_archives" {
  default = 4
  type    = number
}
