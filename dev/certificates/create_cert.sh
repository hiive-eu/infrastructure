#!/usr/bin/env bash

# 1. Generate CA's private key and self-signed certificate
openssl req -x509 -newkey rsa:4096 -days 10000 -nodes -keyout ../secrets/ca-key.pem -out ../secrets/ca-cert.pem -subj "/C=DE/ST=/L=/O=HIIVE/OU=/CN=dev.releases.hiive.buzz/emailAddress=anders@hiive.eu"

echo "CA's self-signed certificate"
openssl x509 -in ../secrets/ca-cert.pem -noout -text

# 2. Generate web server's private key and certificate signing request (CSR)
openssl req -newkey rsa:4096 -nodes -keyout ../secrets/server-key.pem -out ../secrets/server-req.pem -subj "/C=DE/ST=/L=Berlin/O=HIIVE/OU=/CN=dev.releases.hiive.buzz/emailAddress=anders@hiive.eu"

# 3. Use CA's private key to sign web server's CSR and get back the signed certificate
openssl x509 -req -in ../secrets/server-req.pem -days 10000 -CA ../secrets/ca-cert.pem -CAkey ../secrets/ca-key.pem -CAcreateserial -out ../secrets/server-cert.pem -extfile server-ext.cnf

echo "Server's signed certificate"
openssl x509 -in ../secrets/server-cert.pem -noout -text

