#!/usr/bin/env bash

# 3. Use CA's private key to sign web server's CSR and get back the signed certificate
openssl x509 -req -in ../secrets/server-req.pem -days 30 -CA ../secrets/ca-cert.pem -CAkey ../secrets/ca-key.pem -CAcreateserial -out ../secrets/server-cert.pem -extfile server-ext.cnf

echo "Server's signed certificate"
openssl x509 -in ../secrets/server-cert.pem -noout -text


