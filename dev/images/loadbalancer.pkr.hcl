source "hcloud" "loadbalancer_snapshot" {
  token         = var.hcloud_token
  image         = "debian-${var.debian_version_number}"
  location      = var.location
  server_type   = "cx11"
  server_name   = "loadbalancer-dev-packer"
  snapshot_name = "cluster-loadbalancer-dev-${formatdate("DD-MM-YY", timestamp())}"
  ssh_username  = "root"
  snapshot_labels = {
    environment   = "dev"
    public-facing = true
    nomad-server  = false
    consul-client = true
    consul-server = false
    nomad         = var.nomad_version
    consul        = var.consul_version
    fail2ban      = var.fail2ban_version
    #docker        = var.docker_version
  }
}

build {
  name    = "loadbalancer-snapshot-builder"
  sources = ["source.hcloud.loadbalancer_snapshot"]

  provisioner "shell" {
    inline = [
      "echo 'Hello Load Balancer from Packer' >> /etc/motd"
    ]
  }
  provisioner "shell" {
    environment_vars = [
      "DEBIAN_FRONTEND=noninteractive",
    ]

    scripts = [
      "scripts/cluster_prescript.sh",
    ]
  }

  # Before file transfer
  provisioner "shell" {
    environment_vars = [
      "NOMAD_VERSION=${var.nomad_version}",
      "CONSUL_VERSION=${var.consul_version}",
      "DOCKER_VERSION=${local.docker_version_full}",
      "FAIL2BAN_VERSION=${var.fail2ban_version}",
      "DEBIAN_FRONTEND=noninteractive",
    ]

    scripts = [
      "programs/nomad/install.sh",
      "programs/consul/install.sh",
      "programs/docker/install.sh",
      "programs/fail2ban/install.sh",
    ]
  }

  provisioner "file" {
    source      = "programs/bash/files/.bashrc"
    destination = "/root/.bashrc"
  }
  # Nomad
  provisioner "file" {
    source      = "programs/nomad/files/nomad.service"
    destination = "/etc/systemd/system/nomad.service"
  }
  provisioner "file" {
    content = templatefile("programs/nomad/files/nomad_client.pkrtpl",
      {
        datacenter = var.datacenter,
      }
    )
    destination = "/etc/nomad.d/nomad.hcl"
  }

  # Consul
  provisioner "file" {
    source      = "programs/consul/files/consul.service"
    destination = "/etc/systemd/system/consul.service"
  }
  provisioner "file" {
    content = templatefile("programs/consul/files/consul_client.pkrtpl",
      {
        datacenter          = var.datacenter,
        server_private_ipv4 = var.server_node["private_ipv4"],
      }
    )
    destination = "/etc/consul.d/consul.hcl"
  }

  # After file transfer
  provisioner "shell" {
    environment_vars = [
      "DEBIAN_FRONTEND=noninteractive",
    ]

    scripts = [
      "programs/nomad/configure.sh",
      "programs/consul/configure.sh",
      "programs/fail2ban/configure.sh",
    ]
  }
}
