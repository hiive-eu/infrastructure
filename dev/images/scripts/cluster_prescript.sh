#!/usr/bin/env bash
# Update, install tools and setup hashicorp repository
apt-get update -y && apt-get -y upgrade
apt-get install -y gpg coreutils vim dnsmasq

# Hashicorp repo
curl -s https://apt.releases.hashicorp.com/gpg \
  | sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" \
  | sudo tee /etc/apt/sources.list.d/hashicorp.list
apt-get update -y

# https://developer.hashicorp.com/nomad/docs/install/production/requirements
echo "49152 65535" > /proc/sys/net/ipv4/ip_local_port_range

# Enable bridge networking in nomad
curl -L -o cni-plugins.tgz "https://github.com/containernetworking/plugins/releases/download/v1.0.0/cni-plugins-linux-$( [ $(uname -m) = aarch64 ] && echo arm64 || echo amd64)"-v1.0.0.tgz && \
mkdir -p /opt/cni/bin && \
tar -C /opt/cni/bin -xzf cni-plugins.tgz
echo "net.bridge.bridge-nf-call-arptables = 1" >> /etc/sysctl.d/bridge.conf
echo "net.bridge.bridge-nf-call-ip6tables = 1" >> /etc/sysctl.d/bridge.conf
echo "net.bridge.bridge-nf-call-iptables = 1" >> /etc/sysctl.d/bridge.conf

# Use consul DNS
systemctl disable systemd-resolved
systemctl stop systemd-resolved
unlink /etc/resolv.conf
echo "nameserver 127.0.0.1" > /etc/resolv.conf
#mkdir -p /etc/dnsmasq.d/
echo "server=/consul/127.0.0.1#8600" > /etc/dnsmasq.d/10-consul
systemctl enable dnsmasq
systemctl start dnsmasq
