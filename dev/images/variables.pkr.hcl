variable "hcloud_token" {
  sensitive = true
  default   = env("DEV_HCLOUD_TOKEN")
}

variable "debian_version_number" {
  default = "11"
  type    = string
}

variable "debian_version_name" {
  default = "bullseye"
  type    = string
}

# Installed through APT, at time of creation versions in app had a "-1" appended to the version number.
# so nomad version 1.7.5 would be 1.7.5-1 in the APT repository
#
# List versions: apt list -a <PROGRAM>
variable "nomad_version" {
  default = "1.8.3-1"
  type    = string
}
variable "consul_version" {
  default = "1.19.2-1"
  type    = string
}
variable "vault_version" {
  default = "1.17.3-1"
  type    = string
}

variable "docker_version" {
  default = "5:25.0.5-1"
  type    = string
}

locals {
  docker_version_full = "${var.docker_version}~debian.${var.debian_version_number}~${var.debian_version_name}"
}

variable "fail2ban_version" {
  default = "0.11.2-2"
  type    = string
}

variable "nfs_server_version" {
  default = "1:1.3.4-6"
  type    = string
}

variable "s3cmd_version" {
  default = "2.1.0-2"
  type    = string
}

variable "volume_base_name" {
  default = "HC_Volume"
}

