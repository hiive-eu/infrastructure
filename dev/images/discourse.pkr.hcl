source "hcloud" "discourse_snapshot" {
  token         = var.hcloud_token
  image         = "debian-${var.debian_version_number}"
  location      = var.location
  server_type   = "cx11"
  server_name   = "discourse-dev-packer"
  snapshot_name = "discourse-dev-${formatdate("DD-MM-YY", timestamp())}"
  ssh_username  = "root"
  snapshot_labels = {
    environment   = "dev"
    public-facing = true
    discourse     = true
    fail2ban      = var.fail2ban_version
    #docker        = var.docker_version
  }
}

build {
  name    = "discourse-snapshot-builder"
  sources = ["source.hcloud.discourse_snapshot"]

  provisioner "shell" {
    inline = [
      "echo 'Hello Discourse from Packer' >> /etc/motd"
    ]
  }

  # Before file transfer
  provisioner "shell" {
    environment_vars = [
      "DEBIAN_FRONTEND=noninteractive",
    ]

    scripts = [
      "scripts/discourse_prescript.sh",
    ]
  }

  # Before file transfer
  provisioner "shell" {
    environment_vars = [
      "FAIL2BAN_VERSION=${var.fail2ban_version}",
      "DOCKER_VERSION=${local.docker_version_full}",
      "S3CMD_VERSION=${var.s3cmd_version}",
      "DEBIAN_FRONTEND=noninteractive",
    ]
    scripts = [
      "programs/docker/install.sh",
      "programs/fail2ban/install.sh",
      "programs/discourse/install.sh",
      "programs/s3cmd/install.sh"
    ]
  }

  provisioner "file" {
    source      = "programs/bash/files/.bashrc"
    destination = "/root/.bashrc"
  }

  provisioner "file" {
    content = templatefile("programs/discourse/files/config.pkrtpl",
      {
        hostname           = "dev.community.hiive.buzz"
        admin_emails       = "anders@hiive.eu,fabian@hiive.eu,philip@hiive.eu"
        acme_email         = "anders@hiive.eu"
        unicorn_workers    = 4
        login_required     = true
        no_backups_to_keep = var.discourse_number_backups_to_keep_local
        backup_time        = var.discourse_backup_time_utc
        smtp_address       = var.discourse_smtp_address
        smtp_port          = var.discourse_smtp_port
        smtp_username      = var.discourse_smtp_username,
        smtp_password      = var.discourse_smtp_password
        sender_email       = var.discourse_sender_email
        volume_path        = "/mnt/${var.volume_base_name}_${var.discourse_volume_id}/"
      }
    )
    destination = "/var/discourse/containers/app.yml"
  }

  provisioner "file" {
    content = templatefile("programs/s3cmd/files/.s3cfg.pkrtpl",
      {
        access_key       = var.wasabi_access_key
        secret_key       = var.wasabi_secret_key
        bucket_location  = var.wasabi_bucket_location
        host_base        = var.wasabi_host_base
        host_bucket      = var.wasabi_host_bucket
        website_endpoint = var.wasabi_website_endpoint
      }
    )
    destination = var.s3cmd_config_path
  }

  provisioner "file" {
    content = templatefile("programs/discourse/files/discourse_backup.timer.pkrtpl",
      {
        discourse_backup_systemd_job_name = var.discourse_systemd_job_name
        backup_time                       = var.discourse_backup_time_utc
      }
    )
    destination = "/etc/systemd/system/discourse_backup.timer"
  }

  provisioner "file" {
    content = templatefile("programs/discourse/files/discourse_backup.service.pkrtpl",
      {
        backup_script_path = local.discourse_backup_script_path
      }
    )
    destination = "/etc/systemd/system/discourse_backup.service"
  }

  provisioner "file" {
    content = templatefile("programs/discourse/files/discourse_backup.sh.pkrtpl",
      {
        discourse_backup_dir_path = "/mnt/${var.volume_base_name}_${var.discourse_volume_id}/discourse/backups/default/"
        discourse_s3_prefix       = var.discourse_s3_prefix
        s3cmd_config_path         = var.s3cmd_config_path
        bucket_name               = var.wasabi_bucket_name
        no_remote_backups         = var.discourse_no_backups_to_keep_remote
      }
    )
    destination = local.discourse_backup_script_path
  }

  # After file transfer
  provisioner "shell" {
    environment_vars = [
      "DEBIAN_FRONTEND=noninteractive",
    ]
    scripts = [
      "programs/discourse/configure.sh",
    ]
  }
}
