source "hcloud" "server_snapshot" {
  token         = var.hcloud_token
  image         = "debian-${var.debian_version_number}"
  location      = var.location
  server_type   = "cx11"
  server_name   = "server-dev-packer"
  snapshot_name = "cluster-server-dev-${formatdate("DD-MM-YY", timestamp())}"
  ssh_username  = "root"
  snapshot_labels = {
    environment   = "dev"
    public-facing = false
    nomad-client  = true
    nomad-server  = true
    consul-client = false
    consul-server = true
    nomad         = var.nomad_version
    consul        = var.consul_version
    vault         = var.vault_version
    #docker        = var.docker_version
  }
}

build {
  name    = "server-snapshot-builder"
  sources = ["source.hcloud.server_snapshot"]

  provisioner "shell" {
    inline = [
      "echo 'Hello Server from Packer' >> /etc/motd"
    ]
  }

  # Before file transfer
  provisioner "shell" {
    environment_vars = [
      "DEBIAN_FRONTEND=noninteractive",
    ]

    scripts = [
      "scripts/cluster_prescript.sh",
    ]
  }

  provisioner "shell" {
    environment_vars = [
      "NOMAD_VERSION=${var.nomad_version}",
      "CONSUL_VERSION=${var.consul_version}",
      "DOCKER_VERSION=${local.docker_version_full}",
      "VAULT_VERSION=${var.vault_version}",
      "NFS_SERVER_VERSION=${var.nfs_server_version}",
      "DEBIAN_FRONTEND=noninteractive",
    ]
    scripts = [
      "programs/nomad/install.sh",
      "programs/consul/install.sh",
      "programs/vault/install.sh",
      "programs/docker/install.sh",
      "programs/nfs-server/install.sh",
    ]
  }

  provisioner "file" {
    source      = "programs/bash/files/.bashrc"
    destination = "/root/.bashrc"
  }
  # Nomad
  provisioner "file" {
    source      = "programs/nomad/files/nomad.service"
    destination = "/etc/systemd/system/nomad.service"
  }
  provisioner "file" {
    content = templatefile("programs/nomad/files/nomad_server.pkrtpl",
      {
        datacenter = var.datacenter,
      }
    )
    destination = "/etc/nomad.d/nomad.hcl"
  }

  # Consul
  provisioner "file" {
    source      = "programs/consul/files/consul.service"
    destination = "/etc/systemd/system/consul.service"
  }
  provisioner "file" {
    content = templatefile("programs/consul/files/consul_server.pkrtpl",
      {
        datacenter = var.datacenter,
      }
    )
    destination = "/etc/consul.d/consul.hcl"
  }

  # Vault
  provisioner "file" {
    source      = "programs/vault/files/vault.service"
    destination = "/etc/systemd/system/vault.service"
  }
  provisioner "file" {
    content = templatefile("programs/vault/files/vault.pkrtpl",
      {
      }
    )
    destination = "/etc/vault.d/vault.hcl"
  }
  provisioner "file" {
    source      = "programs/vault/files/restore_vault.sh"
    destination = "/opt/vault/restore_vault.sh"
  }

  # After file transfer
  provisioner "shell" {
    environment_vars = [
      "DEBIAN_FRONTEND=noninteractive",
    ]

    scripts = [
      "programs/nomad/configure.sh",
      "programs/consul/configure.sh",
      "programs/vault/configure.sh",
    ]
  }
  provisioner "shell" {
    environment_vars = [
      "DEBIAN_FRONTEND=noninteractive",
    ]

    inline = [
      "chmod +x /opt/vault/restore_vault.sh"
    ]
  }
}
