PS1="\[\e[38;2;228;0;128m\][\u@\h]:\[\e[38;2;225;225;75m\]\w\$\[\e[0m\] ";

# Commands that should be applied only for interactive shells.
[[ $- == *i* ]] || return

HISTFILESIZE=100000
HISTSIZE=10000

shopt -s histappend
shopt -s checkwinsize
shopt -s extglob
shopt -s globstar
shopt -s checkjobs

alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias grep='grep --color=auto'
alias hg='history | grep'
alias l='ls -lFh'
alias la='ls -A'
alias ll='ls -alFh'
alias ls='ls --color=auto'
