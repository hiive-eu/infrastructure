#!/usr/bin/env bash
apt-get -y install consul=$CONSUL_VERSION
consul -autocomplete-install
complete -C /usr/bin/consul consul
