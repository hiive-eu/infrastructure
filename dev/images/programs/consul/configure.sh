#!/usr/bin/env bash
chown --recursive consul:consul /etc/consul.d/
chown --recursive consul:consul /opt/consul/
chmod -R 700 /etc/consul.d/
chmod -R 700 /opt/consul/
chmod -R 600 /etc/consul.d/*

rm /etc/consul.d/consul.env /lib/systemd/system/consul.service
