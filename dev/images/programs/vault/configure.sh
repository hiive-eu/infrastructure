#!/usr/bin/env bash
chown --recursive vault:vault /etc/vault.d/
chown --recursive vault:vault /opt/vault/
chmod -R 700 /etc/vault.d/
chmod -R 700 /opt/vault/
chmod -R 600 /etc/vault.d/*

rm /etc/vault.d/vault.env /lib/systemd/system/vault.service
