#!/usr/bin/env bash
vault operator init -n 1 -t 1 | grep ": " | awk -F ' ' '{ print $4 }' | tee /tmp/vault_temp_tokens.txt
VAULT_TOKEN=$(tail -n 1 /tmp/vault_temp_tokens.txt)
export VAULT_TOKEN
VAULT_TOKEN=$(tail -n 1 /tmp/vault_temp_tokens.txt)
vault operator unseal -non-interactive "$(head -n 1 /tmp/vault_temp_tokens.txt)"
vault operator raft snapshot restore -force /nfs/vault/vault_backup.snap
vault operator unseal -non-interactive "$VAULT_UNSEAL_TOKEN"
