#!/usr/bin/env bash
apt-get install vault=$VAULT_VERSION
vault -autocomplete-install
complete -C /usr/bin/vault vault
setcap cap_ipc_lock=+ep /usr/bin/vault

