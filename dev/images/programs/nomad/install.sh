#!/usr/bin/env bash
apt-get -y install nomad=$NOMAD_VERSION
nomad -autocomplete-install
complete -C /usr/bin/nomad nomad
