chown --recursive nomad:nomad /etc/nomad.d/
chown --recursive nomad:nomad /opt/nomad/
chmod -R 700 /etc/nomad.d/
chmod -R 700 /opt/nomad/
chmod -R 600 /etc/nomad.d/*

rm /etc/nomad.d/nomad.env /lib/systemd/system/nomad.service
