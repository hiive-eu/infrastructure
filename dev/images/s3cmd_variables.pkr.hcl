variable "wasabi_access_key" {
  # secret
  type = string
}
variable "wasabi_secret_key" {
  # secret
  type = string
}
variable "wasabi_bucket_name" {
  default = "dev-backup"
  type    = string
}
variable "wasabi_discourse_prefix" {
  default = "discourse-backups"
  type    = string
}
variable "wasabi_bucket_location" {
  default = "eu-central-2"
  type    = string
}
variable "wasabi_host_base" {
  default = "s3.eu-central-2.wasabisys.com"
  type    = string
}
variable "wasabi_host_bucket" {
  default = "%(bucket)s.s3.eu-central-2.wasabisys.com"
  type    = string
}
variable "wasabi_website_endpoint" {
  default = "http://%(bucket)s.s3-website-%(location)s.amazonaws.com/"
  type    = string
}

variable "discourse_s3_prefix" {
  default = "discourse_backups"
  type    = string
}

variable "s3cmd_config_path" {
  default = "/.s3cfg"
  type    = string
}
