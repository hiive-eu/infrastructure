# Required Environment Variables

Development:

```
DEV_HCLOUD_TOKEN=
```

Production:

```
PROD_HCLOUD_TOKEN=
```

# Build

Build all snapshots:

```
packer build .
```

List available builds:

```
packer inspect .
```

Build a single snapshot:

```
packer build -only '<BUILD>' .
```
Exclude a build:

```
packer build -except '<BUILD>' .
```
