
data "hcloud_image" "loadbalancer" {
  with_selector = "public-facing==true,nomad-server==false,environment==dev"
  most_recent   = true
}

resource "hcloud_server" "loadbalancer" {
  name               = var.loadbalancer_node["node_name"]
  image              = data.hcloud_image.loadbalancer.id
  server_type        = var.loadbalancer_node["node_type"]
  placement_group_id = hcloud_placement_group.spread-placement-group.id
  datacenter         = var.datacenter
  ssh_keys           = [hcloud_ssh_key.dev_hiive.id]
  user_data          = local.loadbalancer_user_data
  public_net {
    ipv4_enabled = true
    ipv6_enabled = false
  }
  labels = {
    "public-facing" = true
    "nomad-server"  = false
    "consul-server" = false
    "snapshot"      = data.hcloud_image.loadbalancer.id
  }
  network {
    network_id = hcloud_network.network.id
    ip         = var.loadbalancer_node["private_ipv4"]
  }
  firewall_ids = [hcloud_firewall.loadbalancer_firewall.id]

  depends_on = [
    hcloud_network_subnet.network-subnet,
    hcloud_firewall.loadbalancer_firewall,
    hcloud_floating_ip.loadbalancer-public-ipv4,
    hcloud_server.server,
    time_sleep.wait_after_server_creation,
  ]
}

