resource "hcloud_ssh_key" "dev_hiive" {
  name       = "dev_hiive"
  public_key = file("${path.root}/public_keys/dev_hiive_key.pub")
}
