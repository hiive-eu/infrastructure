data "hcloud_image" "server" {
  with_selector = "nomad-server==true,environment==dev"
  most_recent   = true
}

resource "hcloud_server" "server" {
  name               = var.server_node["node_name"]
  image              = data.hcloud_image.server.id
  server_type        = var.server_node["node_type"]
  placement_group_id = hcloud_placement_group.spread-placement-group.id
  datacenter         = var.datacenter
  ssh_keys           = [hcloud_ssh_key.dev_hiive.id]
  user_data          = local.server_user_data
  public_net {
    ipv4_enabled = true
    ipv6_enabled = false
  }
  labels = {
    "public-facing" = false
    "nomad-server"  = true
    "consul-server" = true
    "snapshot"      = data.hcloud_image.server.id
  }
  network {
    network_id = hcloud_network.network.id
    ip         = var.server_node["private_ipv4"]
  }
  firewall_ids = [hcloud_firewall.block_wan_firewall.id]
  depends_on = [
    hcloud_network_subnet.network-subnet,
    hcloud_firewall.block_wan_firewall,
  ]
}

resource "time_sleep" "wait_after_server_creation" {
  depends_on = [hcloud_server.server]

  create_duration = "1m"
}
