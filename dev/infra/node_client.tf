data "hcloud_image" "client" {
  with_selector = "nomad-client==true,nomad-server==false,public-facing==false,environment==dev"
  most_recent   = true
}

resource "hcloud_server" "client" {
  count              = length(var.client_nodes)
  name               = var.client_nodes[count.index]["node_name"]
  image              = data.hcloud_image.client.id
  server_type        = var.client_nodes[count.index]["node_type"]
  placement_group_id = hcloud_placement_group.spread-placement-group.id
  datacenter         = var.datacenter
  ssh_keys           = [hcloud_ssh_key.dev_hiive.id]
  user_data          = local.clients_user_data[count.index]
  public_net {
    ipv4_enabled = true
    ipv6_enabled = false
  }
  labels = {
    "public-facing" = false
    "nomad-server"  = false
    "consul-server" = false
    "snapshot"      = data.hcloud_image.client.id
  }
  network {
    network_id = hcloud_network.network.id
    ip         = var.client_nodes[count.index]["private_ipv4"]
  }
  firewall_ids = [hcloud_firewall.block_wan_firewall.id]
  depends_on = [
    hcloud_network_subnet.network-subnet,
    hcloud_firewall.block_wan_firewall,
    hcloud_server.server,
    time_sleep.wait_after_server_creation,
  ]
}
