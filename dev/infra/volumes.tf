resource "hcloud_volume" "persistent-storage" {
  name     = "persistent-storage"
  location = var.location
  size     = 15
  format   = "ext4"

  lifecycle {
    prevent_destroy = true
  }
}

resource "hcloud_volume_attachment" "persistent-storage-attachment" {
  volume_id = hcloud_volume.persistent-storage.id
  server_id = hcloud_server.server.id
  automount = true
}

resource "null_resource" "resize-persistent-storage" {
  triggers = {
    persistent_volume_size = hcloud_volume.persistent-storage.size
  }

  connection {
    type         = "ssh"
    bastion_host = hcloud_server.loadbalancer.ipv4_address
    host         = var.server_node["private_ipv4"]
    user         = "root"
    private_key  = file("${path.root}/private_keys/dev_hiive_key")
  }

  # Resize
  provisioner "remote-exec" {
    inline = [
      "sleep 10",
      "resize2fs \"$(mount | grep ${var.volume_base_name}_${hcloud_volume.persistent-storage.id} | awk '{print $1}')\"",
    ]
  }
}

resource "hcloud_volume" "discourse-storage" {
  name     = "discourse-storage"
  location = var.location
  size     = 20
  format   = "ext4"

  lifecycle {
    prevent_destroy = true
  }
}

resource "hcloud_volume_attachment" "discourse-storage-attachment" {
  volume_id = hcloud_volume.discourse-storage.id
  server_id = hcloud_server.discourse.id
  automount = true
}

resource "null_resource" "resize-discourse-storage" {
  triggers = {
    discourse_volume_size = hcloud_volume.discourse-storage.size
  }

  connection {
    type        = "ssh"
    user        = "root"
    private_key = file("${path.root}/private_keys/dev_hiive_key")
    host        = hcloud_server.discourse.ipv4_address
  }

  # Resize
  provisioner "remote-exec" {
    inline = [
      "sleep 10",
      "resize2fs \"$(mount | grep ${var.volume_base_name}_${hcloud_volume.discourse-storage.id} | awk '{print $1}')\"",
    ]
  }
}
