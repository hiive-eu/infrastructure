data "hcloud_image" "discourse" {
  with_selector = "discourse==true,environment==dev"
  most_recent   = true
}

resource "hcloud_server" "discourse" {
  name               = var.discourse_node["node_name"]
  image              = data.hcloud_image.discourse.id
  server_type        = var.discourse_node["node_type"]
  datacenter         = var.datacenter
  placement_group_id = hcloud_placement_group.spread-placement-group.id
  ssh_keys           = [hcloud_ssh_key.dev_hiive.id]
  user_data          = local.discourse_user_data
  public_net {
    ipv4         = hcloud_primary_ip.discourse_ipv4.id
    ipv6_enabled = false
  }
  labels = {
    "public-facing" = true
    "snapshot"      = data.hcloud_image.discourse.id
  }
  firewall_ids = [hcloud_firewall.discourse_firewall.id]
}
