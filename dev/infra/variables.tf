variable "dev_hcloud_token" {
  sensitive = true
}

variable "floating_ipv4" {
  default = "88.99.122.208"
}

# Hashistack
variable "consul_conf_path" {
  default = "/etc/consul.d/consul.hcl"
}
variable "nomad_conf_path" {
  default = "/etc/nomad.d/nomad.hcl"
}
variable "vault_conf_path" {
  default = "/etc/vault.d/vault.hcl"
}
variable "dev_vault_token" {
  sensitive = true
}
variable "dev_vault_unseal_token" {
  sensitive = true
}

variable "volume_base_name" {
  default = "HC_Volume"
}

variable "nfs_root_share_path" {
  default = "/nfs"
}

locals {
  # cloud-init
  loadbalancer_user_data = templatefile("${path.root}/user-data/loadbalancer.tftpl",
    {
      hostname                  = var.loadbalancer_node["hostname"]
      loadbalancer_private_ipv4 = var.loadbalancer_node["private_ipv4"],
      server_private_ipv4       = var.server_node["private_ipv4"],
      floating_ipv4             = hcloud_floating_ip.loadbalancer-public-ipv4.ip_address,
      consul_conf_path          = var.consul_conf_path,
      nomad_conf_path           = var.nomad_conf_path,
    }
  )
  server_user_data = templatefile("${path.root}/user-data/server.tftpl",
    {
      hostname             = var.server_node["hostname"]
      server_private_ipv4  = var.server_node["private_ipv4"],
      consul_conf_path     = var.consul_conf_path,
      nomad_conf_path      = var.nomad_conf_path,
      vault_token          = var.dev_vault_token,
      vault_unseal_token   = var.dev_vault_unseal_token,
      volume_base_name     = var.volume_base_name,
      persistent_volume_id = hcloud_volume.persistent-storage.id,
      nfs_mount_path       = "/mnt/${var.volume_base_name}_${hcloud_volume.persistent-storage.id}${var.nfs_root_share_path}"
      nfs_share_path       = var.nfs_root_share_path
    }
  )
  clients_user_data = [
    for client in var.client_nodes : templatefile(
      "${path.root}/user-data/client.tftpl",
      {
        hostname            = client["hostname"]
        client_private_ipv4 = client["private_ipv4"],
        server_private_ipv4 = var.server_node["private_ipv4"],
        consul_conf_path    = var.consul_conf_path,
        nomad_conf_path     = var.nomad_conf_path,
      }
    )
  ]
  discourse_user_data = templatefile("${path.root}/user-data/discourse.tftpl",
    {
      hostname = var.discourse_node["hostname"]
    }
  )
}
