output "floating_ipv4" {
  value = hcloud_floating_ip.loadbalancer-public-ipv4.ip_address
}

output "loadbalancer_ipv4" {
  value = hcloud_server.loadbalancer.ipv4_address
}
output "server_ipv4" {
  value = hcloud_server.server.ipv4_address
}
output "client_ipv4s" {
  value = { for client in hcloud_server.client : client.name => client.ipv4_address }
}

output "discourse_ipv4" {
  value = hcloud_server.discourse.ipv4_address
}

output "primary_discourse_ipv4" {
  value = hcloud_primary_ip.discourse_ipv4.ip_address
}

output "discourse_volume_id" {
  value = hcloud_volume.discourse-storage.id
}

output "persistent_volume_id" {
  value = hcloud_volume.persistent-storage.id
}

output "discourse_volume_size" {
  value = hcloud_volume.discourse-storage.size
}

output "persistent_volume_size" {
  value = hcloud_volume.persistent-storage.size
}

# User data
output "loadbalancer_user_data" {
  value     = local.loadbalancer_user_data
  sensitive = true
}
output "server_user_data" {
  value     = local.server_user_data
  sensitive = true
}
output "clients_user_data" {
  value     = local.clients_user_data
  sensitive = true
}
