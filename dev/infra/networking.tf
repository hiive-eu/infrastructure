resource "hcloud_floating_ip" "loadbalancer-public-ipv4" {
  type          = "ipv4"
  name          = "cluster-dev"
  home_location = var.location

  lifecycle {
    prevent_destroy = true
  }
}

resource "hcloud_primary_ip" "discourse_ipv4" {
  name          = "discourse_ipv4"
  type          = "ipv4"
  datacenter    = var.datacenter
  assignee_type = "server"
  auto_delete   = false

  lifecycle {
    prevent_destroy = true
  }
}

resource "hcloud_floating_ip_assignment" "loadbalancer_floating_ip_assignment" {
  floating_ip_id = hcloud_floating_ip.loadbalancer-public-ipv4.id
  server_id      = hcloud_server.loadbalancer.id
}

resource "hcloud_network" "network" {
  name     = "network"
  ip_range = "10.0.0.0/16"
}

resource "hcloud_network_subnet" "network-subnet" {
  type         = "cloud"
  network_id   = hcloud_network.network.id
  network_zone = "eu-central"
  ip_range     = "10.0.1.0/24"
}
