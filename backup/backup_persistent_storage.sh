if [ -z "$BACKUP_DIRS" ]; then
  echo "BACKUP_DIRS environment variable is not set."
  exit 1
fi

if [ -z "$ROOT_DIR_VOLUME_MOUNTS" ]; then
  echo "ROOT_DIR_VOLUME_MOUNTS environment variable is not set."
  exit 1
fi

if [ -z "$BACKUP_VOLUME_NAME" ]; then
  echo "BACKUP_VOLUME_NAME environment variable is not set."
  exit 1
fi

if [ -z "$S3_CONFIG_PATH" ]; then
  echo "S3_CONFIG_PATH environment variable is not set."
  exit 1
fi

if [ -z "$BUCKET_NAME" ]; then
  echo "BUCKET_NAME environment variable is not set."
  exit 1
fi

if [ -z "$NUM_LOCAL_BACKUPS" ]; then
  echo "NUM_LOCAL_BACKUPS environment variable is not set."
  exit 1
fi

if [ -z "$NUM_REMOTE_BACKUPS" ]; then
  echo "NUM_REMOTE_BACKUPS environment variable is not set."
  exit 1
fi

to_epoch() {
    date -d "$1 $2" "+%s"
}

cd "$ROOT_DIR_VOLUME_MOUNTS" || {
  echo "Failed to enter root mount directory: $ROOT_DIR_VOLUME_MOUNTS"
  exit 1
}

for dir_to_backup in $BACKUP_DIRS
do
  echo "Backing up $dir_to_backup"
  archive_size=$(tar -czf - "$dir_to_backup" | wc -c) 
  archive_size_mb=$((archive_size / 1024 / 1024))
  archive_size_gb=$((archive_size / 1024 / 1024 / 1024))
  required_free_space_gb=$((archive_size_gb + 5))
  free_space_gb=$(df -BG "$ROOT_DIR_VOLUME_MOUNTS$BACKUP_VOLUME_NAME" | awk 'NR==2{print $4}' | sed 's/G//')
  echo "Size of backup archive: $archive_size_mb MB"
  echo "Required free space: $required_free_space_gb GB"
  echo "Free space: $free_space_gb GB"

  timestamp=$(date +"%Y-%m-%d-%H%M%S")
  archive_name="$dir_to_backup-$timestamp.tar.gz"
  backup_dir="$ROOT_DIR_VOLUME_MOUNTS$BACKUP_VOLUME_NAME/$dir_to_backup/"
  mkdir -p "$backup_dir"
  tar -czf "$backup_dir$archive_name" "$dir_to_backup"
  echo "Created archive: $backup_dir$archive_name"

  s3cmd --config="$S3_CONFIG_PATH" put "$backup_dir$archive_name" s3://"$BUCKET_NAME"/"$dir_to_backup"_backups/"$archive_name"

  local_backup_archives=$(find $backup_dir -type f | sort)
  total_local_files=$(echo "$local_backup_archives" | wc -l)
  local_files_to_select=$((total_local_files - NUM_LOCAL_BACKUPS))
  echo "total local files: $total_local_files"
  echo "local files to select: $local_files_to_select"

  echo "$local_backup_archives" | head -n $local_files_to_select | while read -r old_local_file; do
    echo "Delete $old_local_file"
    rm $old_local_file
  done

  remote_backup_archives=$(s3cmd --config="$S3_CONFIG_PATH" ls s3://"$BUCKET_NAME"/"$dir_to_backup"_backups/)
  total_remote_files=$(echo "$remote_backup_archives" | wc -l)
  remote_files_to_select=$((total_remote_files - NUM_REMOTE_BACKUPS))
  echo "total remote files: $total_remote_files"
  echo "remote files to select: $remote_files_to_select"

  echo "$remote_backup_archives" | awk '{print $1, $2, to_epoch $1 $2, $4}' | sort -k3n | head -n $remote_files_to_select | awk '{print $4}' | while read -r old_remote_file; do
    s3cmd --config="$S3_CONFIG_PATH" del "$old_remote_file"
  done

  echo ""
done
