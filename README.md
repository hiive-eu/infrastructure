# *** NOT PRODUCTION READY ***

Known shortcomings:

* Make sure that licenses of used packages are not violated
* Need to implement database replication and failover
* SSH key for the development environment is too small
* Encrypt backups

Unknown shortcomings: most likely.

# Infrastructure overview

This repository contains a directory for the [development environment](https://gitlab.com/hiive-eu/infrastructure/-/tree/main/dev),
and a directory for the [production environment](https://gitlab.com/hiive-eu/infrastructure/-/tree/main/prod).

In directory for the development environment there is a Terraform project called `infra` and a Terraform project called `nomad`,
and a Packer project called `images`.

Because Discourse is the only service running in the production environment, most of the information here applies to the development environment
including the following graph that depicts the architecture of system:

![Infrastructure overview graph](docs/images/infrastructure.png)

# Configuration, setup and deployment

## Required secrets

Replace `<ENV>` with the environment e.g. `dev` or `prod`

### SSH keys

`infra/private_keys/<ENV>_hiive_key` (Private SSH key)

`infra/public_keys/<ENV>_hiive_key.pub` (Public SSH key)

### Environment variables

`<ENV>_HCLOUD_TOKEN`

`TF_VAR_<ENV>_hcloud_token`

`TF_VAR_<ENV>_vault_token`

`TF_VAR_<ENV>_vault_unseal_token`

`TF_VAR_root_wasabi_secret_key` (Distinguish between dev and prod in future)

`TF_VAR_root_wasabi_access_key` (Distinguish between dev and prod in future)

### Vault KV

secret/default/api/brevo_token

secret/default/api/pg_password

secret/default/api/pg_username

secret/default/api/secretkey

secret/default/cluster_backup/wasabi_access_key 

secret/default/cluster_backup/wasabi_secret_key 

secret/default/labtest/token 

secret/default/hiive_password

secret/default/hiive_username

secret/default/pg_password

secret/default/pg_username

secret/default/traefik/server_cert

secret/default/traefik/server_key

secret/default/traefik/traefik_basicauth_password_hash

secret/default/traefik/traefik_basicauth_user 

## Initialize and deploy

### Initialize project

Log in to terraform cloud:

```
terraform login
```

Initialize the terraform project:

```
terraform init
```

Upload and check the configuration;

```
terraform plan
```

Apply the configuration

```
terraform apply
```

## Configure Vault

When initializing a fresh instance of vault with no backup then the following steps must be completed

### Initialize and unseal Vault

Initialize and unseal the vault. Store the root token and the unseal tokens in a safe place

## setup nomad workloads for vault

Run on nomad server to configure vault to use nomad workloads:

```
nomad setup vault -jwks-url http://<NOMAD_SERVER_IPV4>:4646/.well-known/jwks.json -y
```

### Setup kv secret engine and policies

Enable key/value secret engine:

```
vault secrets enable -path='secret' -version='2' 'kv'
```

Add value for keys 'user' and 'password' for job 'testjob'. 'default' is required, 'config' is not.

```
vault kv put -mount 'secret' 'default/testjob/config' 'password=password' 'user=username'
```

The automatic setup might have different paths than the jobs are using, check them with:

```
vault policy read  nomad-workloads
```

### Backup Vault

Create a backup backup. Run the following command on the node where Vault is running:

```
vault operator raft snapshot save /nfs/vault/vault_backup.snap
```

# Nomad

The terraform nomad provider needs to access the load-balancer node through a local ssh port forward:

```
ssh -L 127.0.0.1:4646:127.0.0.1:4646 -N root@<LOAD_BALANCER_IPV4>
```

# cloud-init

Nodes are partly provisioned using `cloud-init`. In case there are problems with the config file, it can be debugged using `journalctl`:

```
journalctl -xeu cloud-init.service
```

# Generate CA, create server CSR, sign server CSR with CA's key and produce server certificate

Edit the script `certificates/create_cert.sh` by updating the following:

* CA days
* CA information
* server CSR information
* Server certificate days

```
certificates/create_cert.sh
```

`server_cert.pem` and `server_key.pem` are imported into Traefik's vault secrets.

`ca_cert.pem` is served by the webserver.

# Connecting to nodes

The load balancer also behaves as a bastion server. When the nodes are up you can connect to the private nodes using their
private IP-address by jumping through the load balancer:


```
ssh -J root@<LOADBALANCER_PUBLIC_IP> root@<NODE_PRIVATE_IP>
```

# Backup

Backups of every NFS drive is done once a week and uploaded to Wasabi

# Discourse

Build and run Discourse:

```
cd /var/discourse
./launcher rebuild app
```

Start from backup:

(backup location inside docker: `/shared/backups/default/`)

```
cd /var/discourse
./launcer rebuild app
./launcher enter app

discourse enable_restore
discourse restore <NAME_OF_BACKUP>
exit

./launcher rebuild app
```
