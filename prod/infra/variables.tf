variable "prod_hcloud_token" {
  sensitive = true
}

variable "volume_base_name" {
  default = "HC_Volume"
}

locals {
  # cloud-init
  discourse_user_data = templatefile("${path.root}/user-data/discourse.tftpl",
    {
      hostname = var.discourse_node["hostname"]
    }
  )
}
