resource "hcloud_ssh_key" "prod_hiive" {
  name       = "prod_hiive"
  public_key = file("${path.root}/public_keys/prod_hiive_key.pub")
}
