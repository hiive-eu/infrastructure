#!/usr/bin/env bash

# Destroys all hcloud servers.
# Filtering servers can be done by providing an argument that will be filtered with grep

while read -r server; do
  terraform destroy -auto-approve -target="$server"
done < <(terraform state list | grep hcloud_server | grep "$1")
