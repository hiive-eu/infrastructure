resource "hcloud_primary_ip" "discourse_ipv4" {
  name          = "discourse_ipv4"
  type          = "ipv4"
  datacenter    = var.datacenter
  assignee_type = "server"
  auto_delete   = false

  lifecycle {
    prevent_destroy = true
  }
}
