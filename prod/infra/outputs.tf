output "discourse_ipv4" {
  value = hcloud_server.discourse.ipv4_address
}

output "primary_discourse_ipv4" {
  value = hcloud_primary_ip.discourse_ipv4.ip_address
}

output "discourse_volume_id" {
  value = hcloud_volume.discourse-storage.id
}

output "discourse_volume_size" {
  value = hcloud_volume.discourse-storage.size
}
