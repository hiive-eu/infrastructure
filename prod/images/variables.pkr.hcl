variable "hcloud_token" {
  sensitive = true
  default   = env("PROD_HCLOUD_TOKEN")
}

variable "debian_version_number" {
  default = "11"
  type    = string
}

variable "debian_version_name" {
  default = "bullseye"
  type    = string
}

# List versions: apt list -a docker-ce
variable "docker_version" {
  default = "5:25.0.5-1"
  type    = string
}

locals {
  docker_version_full = "${var.docker_version}~debian.${var.debian_version_number}~${var.debian_version_name}"
}

variable "fail2ban_version" {
  default = "0.11.2-2"
  type    = string
}

variable "s3cmd_version" {
  default = "2.1.0-2"
  type    = string
}

variable "volume_base_name" {
  default = "HC_Volume"
}
