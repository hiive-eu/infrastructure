variable "discourse_backup_time_utc" {
  default = "Sun *-*-* 02:00:00"
  type    = string
}
variable "discourse_sender_email" {
  default = "community@hiive.eu"
  type    = string
}
variable "discourse_smtp_address" {
  default = "smtp-relay.brevo.com"
  type    = string
}
variable "discourse_smtp_port" {
  default = 587
  type    = number
}
variable "discourse_smtp_username" {
  # secret
  type = string
}
variable "discourse_smtp_password" {
  # secret
  type = string
}
variable "discourse_systemd_job_name" {
  default = "discourse_backup"
  type    = string
}

variable "discourse_number_backups_to_keep_local" {
  default = 2
  type    = number
}

variable "discourse_no_backups_to_keep_remote" {
  default = 4
  type    = number
}

locals {
  discourse_backup_script_path = "/opt/discourse_backup.sh"
}
