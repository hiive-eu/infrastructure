variable "location" {
  default = "fsn1"
  type    = string
}

variable "datacenter" {
  default = "fsn1-dc14"
  type    = string
}

variable "discourse_volume_id" {
  default = "100668273"
  type    = string
}

variable "discourse_node" {
  type = map(string)
  default = {
    hostname  = "discourse"
    node_name = "discourse"
    node_type = "cx22"
  }
}
